import { Component, OnInit } from '@angular/core';
import { LogicService, Product } from '../logic.service';

@Component({
  selector: 'app-kart',
  templateUrl: './kart.component.html',
  styleUrls: ['./kart.component.scss']
})
export class KartComponent implements OnInit {

  products:Product[];
  qrvalue:string;

  constructor() { }

  ngOnInit(): void {
    this.products = LogicService.orders;
    console.log(this.products.length);
  }

  generateCode(payed:boolean){

    let price = 0.1;
    for(let p of this.products){
      price += p.price;
    }

    let data = {
      "user": payed ? "Robin Naumann" : "Lukas Bormann",
      "payed": false,
      "price": price,
      "items": this.products.map(e => {return "1x " + e.name})
    }
    this.qrvalue = JSON.stringify(data);
    
  }

}
