import { Component, OnInit } from '@angular/core';
import { LogicService, Product } from '../logic.service';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

  products:Product[];

  constructor() { }

  ngOnInit(): void {
    this.products = LogicService.products;
  }

}
