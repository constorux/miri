import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { KartComponent } from './kart/kart.component';
import { ProductComponent } from './product/product.component';
import { ShopComponent } from './shop/shop.component';

const routes: Routes = [{ path: '', component: HomeComponent }, { path: 'product/:id', component: ProductComponent }, { path: 'kart', component: KartComponent },{ path: 'shop', component: ShopComponent },];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
