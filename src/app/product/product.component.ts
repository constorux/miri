import { Component, OnInit } from '@angular/core';
import { LogicService, Product } from '../logic.service';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  product: Product;

  constructor(private route: ActivatedRoute) { }

  public token = "";
  ngOnInit() {
    let token = this.route.snapshot.paramMap.get("id");
    this.product = LogicService.getProduct(token);
  }

  isInCart():boolean {
    return LogicService.isInCart(this.product);
  }  

  toggleCart() {
    LogicService.toggleCart(this.product);
  }

}
