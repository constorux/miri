import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class Product {
  constructor(public image: string, public price: number, public name: string) { }
}

export class LogicService {

  static products: Product[] = [
    new Product("https://img.rewe-static.de/8014432/32212673_digital-image.png?output-quality=60&fit=inside|840:840&output-format=image/webp&background-color=ffffff",1.5,"FAIRolade"),
    new Product("https://img.rewe-static.de/3380710/31502315_digital-image.png?output-quality=60&fit=inside|840:840&output-format=image/webp&background-color=ffffff",2.5,"Thailändisches Curry"),
    new Product("https://img.rewe-static.de/0949627/28306000_digital-image.png?output-quality=60&fit=inside|840:840&output-format=image/webp&background-color=ffffff",0.7,"Orangensaft"),
    new Product("https://img.rewe-static.de/8445523/31210138_digital-image.png?output-quality=60&fit=inside|840:840&output-format=image/webp&background-color=ffffff",2.7,"TeriYaki Sauce"),
    new Product("https://img.rewe-static.de/8422794/31408549_digital-image.png?output-quality=60&fit=inside|840:840&output-format=image/webp&background-color=ffffff",1.0,"Eistee Minze")
  ];
  static orders: Product[] = [];

  constructor() { }

  static getProduct(name:string):Product{
    return LogicService.products.find((e) => e.name === name);
  }

  static isInCart(p: Product): boolean {
    return LogicService.orders.find(e => e.name === p.name) != undefined;
  }

  static toggleCart(p: Product): void {
    if(!this.isInCart(p)){
      LogicService.orders.push(p);
    }
    else{
      this.orders = LogicService.orders.filter((e) => e.name != p.name);
    }

    console.log(this.orders.length);
  }
}
